//
//  InputProperties.h
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __Sandman__InputProperties__
#define __Sandman__InputProperties__

#include <iostream>
#include "PhysProperties.h"

using namespace std;

#ifdef APPLE
#pragma mark Direction
#endif
enum direction_e{
    DIRECTION_UP,
    DIRECTION_DOWN,
    DIRECTION_RIGHT,
    DIRECTION_LEFT,
    DIRECTION_NONE,
    DIRECTION_NULL
};

#ifdef APPLE
#pragma mark Button State
#endif
enum button_state{
    BUTTON_PRESSED,
    BUTTON_HELD,
    BUTTON_RELEASED,
    BUTTON_ENABLED,
    BUTTON_DISABLED
};



/*
 ===============================================================================
 
 button_t
 Button type containing button state and name for referall
 
 ===============================================================================
 */
struct button_t{
    button_state    state = BUTTON_RELEASED;
    string          name;
    vec2_t          position;
    
    void            Press( void );
    void            Release( void );

    const bool      Active( void ) const;
    const bool      SinglePress( void ) const;
    const bool      Pressed( void ) const;
    const bool      Held( void ) const;
    const bool      Released(void ) const;

    
    void            Enable( void );
    void            Disable( void );
    
    void            Clear( void );
    void            PrintOut( void );
};


struct direction_t{
                    direction_t();
    
    direction_e     direction;
    
    void            PrintOut( void );
};

/*
 ===============================================================================
 
 controller_t
 Contains a group of buttons, may be changed later
 
 ===============================================================================
 */
struct controller_t{
    button_t up,down,left,right,button1,button2,fullScreenButton;
    
                    controller_t();
    void            PrintOut( void );
    const bool      NoDirectionPressed( void ) const;
    
};

#endif /* defined(__Sandman__InputProperties__) */


























