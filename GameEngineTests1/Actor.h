//
//  Actor.h
//  Sandman
//
//  Created by Evan Chapman on 1/10/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __Sandman__Actor__
#define __Sandman__Actor__

#include <iostream>
#include "PrefixHeader.pch"
#include "TextureLibrary.h"
#include <random>




const int MAX_ANIMATIONS = 100;

/*
 ===============================================================================
 
 Actor.h
 Contains any visible items
 
 ===============================================================================
 */
class Actor{
public:
                        Actor();
                        ~Actor();
    
    void                Init( void );
    void                Update( const Uint32 delta );
    void                Render( void );
    
    bool                SetTextureLibrary( TextureLibrary* textureLib);
    bool                SetRenderer( SDL_Renderer* renderer );
    bool                SetTextureWithIndex( const int index );
    bool                SetTextureWithFile( const string file );
    
    void                SetDimensions( dimensions_t newDimensions );
    dimensions_t*       GetDimensions( void );
    void                SetSource( dimensions_t newSource );
    void                SetBoundingBox( dimensions_t newBox );
    void                SetTint( color_t newTint );
    void                SetCharacterNum( const int newCharacterNum );
    
    void                AttachController( controller_t* newController );
    void                DetachController( void );
    
    void                SetDirection( direction_e newDirection );
    
    void                SetMomentum( const momentum_t newMomentum );
    momentum_t*         GetMomentum( void );
    
    void                CheckCollisions( const Actor* rhs );
    void                PrintOut( void );
    
private:
    
    SDL_Renderer*       pCurrRenderer;//Passed in from gameapp
    TextureLibrary*     pCurrTextureLib;//For rendering
    int                 texture;//Index of texture in lib
    
    double              angle = 0;//Used when rendercopy
    SDL_Point           center{0,0};
    SDL_RendererFlip    flip = SDL_FLIP_NONE;
    
    color_t             tint;
    
    dimensions_t        source;//For render source
    dimensions_t        dimensions;//For position + motion
    dimensions_t        boundingBox; //for collisions
    
    controller_t*       controller;
    momentum_t          momentum;
    bool                isMoving;
    direction_t         direction;
    
    int                 characterNum = 0;
    int                 currFrame = 0;
    const int           lastFrame = 3;
    
    int                 animTickSpeed = 10;
    int                 animTickTotal = 0;
    int                 animTickMax = 2000;
    
    int                 xSize = 32;
    int                 ySize = 32;
    const int           maxColumns    = 7;
    const int           maxRows       = 8;
    const int           columnStride = 3;
    const int           rowStride = 4;
    int                 column  = 0;
    int                 row     = 0;
    int*                currAnimCol;
    int*                currAnimRow;
    int                 upAnimCol[3]{0,1,2};
    int                 upAnimRow[3]{0,3,0};
    int                 downAnimCol[3]{2,2,2};
    int                 downAnimRow[3]{1,2,3};
    int                 rightAnimCol[3]{1,1,1};
    int                 rightAnimRow[3]{0,1,2};
    int                 leftAnimCol[3]{0,0,0};
    int                 leftAnimRow[3]{2,1,3};
    
    vec2_t              lastMove;
    
    
private:
    void                HandleInput( const Uint32 delta );
    void                UpdateMovement( const Uint32 delta );
    void                UpdateAnimation( const Uint32 delta );
    void                Correct( const Uint32 delta );
    void                UpdateBoundingBox( void );
};



#endif /* defined(__Sandman__Actor__) */






























