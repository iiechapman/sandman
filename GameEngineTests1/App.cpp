//
//  App.cpp
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/30/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include "App.h"
using namespace std;

int App::init( int width, int height, string title )
{
    SDLApp::init( width,height,title );
    return APP_OK;
}


void App::destroy()
{
    SDLApp::destroy();
}


int App::run( int width,int height ,string title )
{
    SDLApp::run( width,height,title );
    return APP_OK;
}


void App::HandleEvents( SDL_Event* ev )
{
    SDLApp::HandleEvents( ev );
}


void App::Render()
{
    SDLApp::Render();
}
