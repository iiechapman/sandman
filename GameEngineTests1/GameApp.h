//
//  GameApp.h
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/30/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#ifndef __GameEngineTests1__GameApp__
#define __GameEngineTests1__GameApp__


#include <iostream>
#include <vector>
#include "App.h"
#include "PrefixHeader.pch"
#include "Actor.h"
#include "TextureLibrary.h"
#include "tinyxml2.h"

using namespace tinyxml2;
using namespace std;

#define MAX_ACTORS 100000

/*
 ===================================
 GameApp
 Entire Game loop enclosed in code
 ===================================
 */

class GameApp : public App
{
public:
                        GameApp();
                        ~GameApp();
    
    const int           Init( void );
    const int           LoadAssetts( void );
    
    const int           Run( int width, int height, string title );
    void                HandleEvents( SDL_Event* ev );
    void                Render( void );
    void                Destroy( void );
    

private:
    TextureLibrary*     textureLib;
    int                 totalActors = 10;
    Actor*              testActor[MAX_ACTORS];
    Timer               timer;

    XMLDocument*        doc;
    
    Uint32              framesPerSecond;
    Uint32              framesRendered;
    Uint32              targetFrameRate = 60;
    
    bool                paused = false;
    
    bool                fullScreen = false;
    SDL_WindowFlags     windowMode;
    
    //Test objects
    int                 backTexture;
    int                 spriteTexture;

    
    float               scale = 1;
    bool                loadScripts;
    
    
    SDL_Rect            backDim; // convert to tile class that shares with actor
    
    double              angle   {0.0f};
    SDL_Point           center  {0,0};
    SDL_RendererFlip    flip    {SDL_FLIP_NONE};
    SDL_Rect            source;
    
    int                 characterNum = 0;
    int                 currFrame = 0;
    const int           lastFrame = 3;

    int                 animTickSpeed = 10;
    int                 animTickTotal = 0;
    int                 animTickMax = 2000;
    
    float               playerPosX = 200;
    float               playerPosY = 300;
    
    momentum_t          momentum;
    
    
    bool                pressUp,pressDown,pressLeft,pressRight;
    controller_t*       controller;
    
    int                 xSize = 32;
    int                 ySize = 32;
    const int           maxColumns    = 7;
    const int           maxRows       = 8;
    const int           columnStride = 3;
    const int           rowStride = 4;
    int                 column  = 0;
    int                 row     = 0;
    int*                currAnimCol;
    int*                currAnimRow;
    int                 upAnimCol[3]{0,1,2};
    int                 upAnimRow[3]{0,3,0};
    int                 downAnimCol[3]{2,2,2};
    int                 downAnimRow[3]{1,2,3};
    int                 rightAnimCol[3]{1,1,1};
    int                 rightAnimRow[3]{0,1,2};
    int                 leftAnimCol[3]{0,0,0};
    int                 leftAnimRow[3]{2,1,3};
    //End Test Objects
    
    
private:
    void                LoadXML( void );
    void                InitTextureHandling( void );
    
    void                SetupTestData( void );
    void                SetupTimer( void );
    void                SetupTextureLibrary( void );
    
    void                CheckTestControls( void );
    void                CharacterSwitch( void );
    void                UpdateAnimation( void );
    
    void                CreateActors( void );
    void                UpdateActors( void );
    void                RenderActors( void );
    void                DestroyActors( void );
    void                CheckActorBounds( void );
    
    void                CheckForFullScreen( void );
    void                CalculateFPS( void );
    void                RegulateFrameRate( void );
    
    void                HandleKeyboard( void );
    void                HandleLogic( void );
    void                RandomizeMovement( void );

};


#endif /* defined(__GameEngineTests1__GameApp__) */




































