//
//  Definitions.h
//  TestingInterfaces1
//
//  Created by Evan Chapman on 12/9/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

/*
 ===================================
 Misc Definitions
 Used as Precompiled header
 ===================================
 */


#ifndef __TestingInterfaces1__Definitions__
#define __TestingInterfaces1__Definitions__


#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768

#define PI 3.14159265

#include <SDL2/SDL.h>

void PrintRect(SDL_Rect rect);



#endif /* defined(__TestingInterfaces1__Definitions__) */



























































