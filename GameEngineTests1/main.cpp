#include <SDL2/SDL.h>
#include "GameApp.h"
#include "PrefixHeader.pch"

using namespace std;


/*
 ===============================================================================
 
    Main
 
 ===============================================================================
 */

int main(int argc, char* argv[])
{
    //Creates App object and runs app loop
    string      gAppTitle = "Sandman";
    GameApp     App;
    return      App.Run(SCREEN_WIDTH, SCREEN_HEIGHT, gAppTitle);
}