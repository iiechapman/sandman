//
//  PhysProperties.h
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __Sandman__PhysProperties__
#define __Sandman__PhysProperties__

#include <iostream>
#include <SDL2/SDL.h>

struct linear_float_t{
    float           value;
    float           increaseValue;
    float           decreaseValue;
    
    float           operator=( const float rhs );
    linear_float_t* operator=( const linear_float_t rhs );
    
    void            SetChangeValue( const float newValue );
    void            SetIncreaseValue( const float newIncreaseValue );
    void            SetDecreaseValue( const float newDecreaseValue );
    
    void            Increase( void );
    void            Decrease( void );
    
    void            PrintOut( void ) const;
};


/*
 ===============================================================================
 
 Bounds
 Describes a number with a max and a min
 
 ===============================================================================
 */

struct bounds_t{
    float  max;
    float  min;
    linear_float_t  current;
    
    void            SetMin( const float newMin );
    void            SetMax( const float newMax );
    void            Set( const float newValue );
    
    void            Increase( void );
    void            Decrease( void );
    
    void            CheckBounds( void );
    
    void            Clear( void );
    void            PrintOut( void ) const;
    bounds_t*       operator=( const bounds_t rhs );
};



/*
 ===============================================================================
 
 vec2_t
 Structure for 2-Dimensional vector
 
 ===============================================================================
 */
struct vec2_t{
    bounds_t        x,y;
    
    
    void            operator=( const vec2_t rhs );
    
    void            Clear ( void );
    void            PrintOut( void ) const;
};



/*
 ===============================================================================
 
 momentum_t
 Speed structure that handles clamping and acceleration
 
 ===============================================================================
 */
struct momentum_t{
    bounds_t        speed;
    bounds_t        accel;
    bounds_t        decel;
    
    void            Accelerate( const Uint32 delta );
    void            Decelerate( const Uint32 delta );
    void            Stop( void );
    
    void            Update( const Uint32 delta );
    
    const float     GetSpeed( void ) const;
    const bool      InMotion( void ) const;
    
    bounds_t*       Accel( void );
    bounds_t*       Decel( void );
    bounds_t*       Speed( void );
    
    void            Clear( void );
    void            PrintOut( void ) const;
    momentum_t*     operator=( const momentum_t rhs );
};



/*
 ===============================================================================
 
 dimensions_t
 Adapter for SDL_Rect thats more flexible
 
 ===============================================================================
 */
struct dimensions_t{
                    dimensions_t();
        
    float           x;
    float           y;
    float           w;
    float           h;
    
    float           left;
    float           right;
    float           top;
    float           bottom;
    
    SDL_Rect        rect;
    
    void            operator=( const dimensions_t rhs );

    SDL_Rect        ToRect( void );
    
    void            CalculateSides( void );
    
    const bool      OverlapVertical( dimensions_t rhs );
    const bool      OverlapHorizontal( dimensions_t rhs );
    const bool      OverlapWithDimensions( dimensions_t rhs );
    
    const vec2_t*   OverlapDepth( const dimensions_t rhs );
    
    void            Clear( void );
    void            PrintOut( void ) const;
};

#endif /* defined(__Sandman__PhysProperties__) */

































