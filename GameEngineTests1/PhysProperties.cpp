//
//  PhysProperties.cpp
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "PhysProperties.h"
#include <iostream>
using namespace std;



#ifdef APPLE
#pragma mark linear_float_t
#endif
/*
 ====================
 linear_float_t::SetIncreaseValue
 ====================
 */
void linear_float_t::SetIncreaseValue( const float newIncreaseValue ){
    increaseValue = newIncreaseValue;
}



/*
 ====================
 linear_float_t::SetDecreaseValue
 ====================
 */
void linear_float_t::SetDecreaseValue( const float newDecreaseValue ){
    decreaseValue = newDecreaseValue;
}


/*
 ====================
 linear_float_t::SetChangeValue
 ====================
 */
void linear_float_t::SetChangeValue( const float newValue ){
    decreaseValue = newValue;
    increaseValue = newValue;
}



/*
 ====================
 linear_float_t::operator=float
 ====================
 */
float linear_float_t::operator=( float rhs ){
    value = rhs;
    return value;
}


/*
 ====================
 linear_float_t::operator=linear_float_t
 ====================
 */
linear_float_t* linear_float_t::operator=( const linear_float_t rhs ){
    value = rhs.value;
    decreaseValue = rhs.decreaseValue;
    increaseValue = rhs.increaseValue;

    return this;
}


/*
 ====================
 linear_float_t::Increase
 ====================
 */
void linear_float_t::Increase( void ){
    value += increaseValue;
}



/*
 ====================
 linear_float_t::Decrease
 ====================
 */
void linear_float_t::Decrease( void ){
    value -= decreaseValue;
}



/*
 ====================
 linear_float_t::PrintOut
 ====================
 */
void linear_float_t::PrintOut( void ) const{
    cout << "Value: " << value << endl;
    cout << "Increase Value: " << increaseValue << endl;
    cout << "Decrease Value: " << decreaseValue << endl;
}

#ifdef APPLE
#pragma mark vec2_t
#endif
/*
 ====================
 vec2_t::Clear
 ====================
 */
void vec2_t::Clear( void ){
    x.current.value = 0;
    y.current.value = 0;
}


/*
 ====================
 vec2_t::Printout
 ====================
 */
void vec2_t::PrintOut( void ) const{
    cout << "X: ";
    x.PrintOut();
    cout << "Y: ";
    y.PrintOut();
}



#ifdef APPLE
#pragma mark bounds_t
#endif
/*
 ====================
 bounds_t::Set
 ====================
 */
void bounds_t::Set( const float newValue ){
    current.value = newValue;
    CheckBounds();
}



/*
 ====================
 bounds_t::SetMin
 ====================
 */
void bounds_t::SetMin( const float newMin ){
    min = newMin;
    CheckBounds();
}



/*
 ====================
 bounds_t::SetMax
 ====================
 */
void bounds_t::SetMax( const float newMax ){
    max = newMax;
    CheckBounds();
}



/*
 ====================
 bounds_t::Increase
 ====================
 */
void bounds_t::Increase( void ){
    current.Increase();
}



/*
 ====================
 bounds_t::Decrease
 ====================
 */
void bounds_t::Decrease( void ){
    current.Decrease();
}



/*
 ====================
 bounds_t::CheckBounds
 ====================
 */
void bounds_t::CheckBounds( void ){
    if (current.value > max){
        current.value = max;
    }
    
    if ( current.value < min ){
        current.value = min;
    }
}



/*
 ====================
 bounds_t::Clear
 ====================
 */
void bounds_t::Clear( void ){
    max = 0;
    min = 0;
    current.value = 0;
}



/*
 ====================
 bounds_t::PrintOut
 ====================
 */
void bounds_t::PrintOut( void ) const{
    current.PrintOut();
    cout << "Max: " << max << endl;
    cout << "Min: " << min << endl;
}


/*
 ====================
 bounds_t::operator=
 ====================
 */
bounds_t* bounds_t::operator=( const bounds_t rhs ){
    current = rhs.current;
    max = rhs.max;
    min = rhs.min;
    
    return this;
}


#ifdef APPLE
#pragma mark dimensions_t
#endif

/*
 ====================
 dimensions_t::dimensions_t
 ====================
 */
dimensions_t::dimensions_t(){
    Clear();
}



/*
 ====================
 dimensions_t::ToRect
 ====================
 */
SDL_Rect dimensions_t::ToRect( void ){
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    
    return rect;
}



/*
 ====================
 dimensions_t::PrintOut
 ====================
 */
void dimensions_t::PrintOut( void ) const{
    printf("X: %f \n" , x);
    printf("Y: %f \n" , y);
    printf("W: %f \n" , w);
    printf("H: %f \n" , h);
    
    printf("Left: %f \n" , left);
    printf("Right: %f \n" , right);
    printf("Top: %f \n" , top);
    printf("Bottom: %f \n" , bottom);
}


/*
 ====================
 dimensions_t::operator=dimensions_t
 ====================
 */
void dimensions_t::operator=( const dimensions_t rhs ){
    x = rhs.x;
    y = rhs.y;
    w = rhs.w;
    h = rhs.h;
    CalculateSides();
}



/*
 ====================
 dimensions_t::Clear
 ====================
 */
void dimensions_t::Clear( void ){
    x = 0;
    y = 0;
    w = 0;
    h = 0;
    CalculateSides();
}


/*
 ====================
 dimensions_t::CalculateSides
 ====================
 */
void dimensions_t::CalculateSides( void ){
    left = x;
    right = left + w;
    top = y;
    bottom = top + h;
}


/*
 ====================
 dimensions_t::OverlapHorizontalWithDimension
 ====================
 */
const bool dimensions_t::OverlapHorizontal( dimensions_t rhs ){
    CalculateSides();
    rhs.CalculateSides();
    
    if (right < rhs.left ||
        left > rhs.right){
        return false;
    }
    return true;
}


/*
 ====================
 dimensions_t::OverlapVerticalWithDimensions
 ====================
 */
const bool dimensions_t::OverlapVertical( dimensions_t rhs ){
    CalculateSides();
    rhs.CalculateSides();
    
    if (bottom < rhs.top ||
        top > rhs.bottom){
        return false;
    }
    return true;
}


/*
 ====================
 dimensions_t::OverlapWithDimensions
 ====================
 */
const bool dimensions_t::OverlapWithDimensions( dimensions_t rhs ){
    CalculateSides();
    rhs.CalculateSides();
    
    if ( OverlapHorizontal( rhs ) && OverlapVertical( rhs ) ){
        return true;
    } else {
        return false;
    }
}


/*
 ====================
 dimensions_t::OverlapDepthWithDimensions
 ====================
 */
const vec2_t* dimensions_t::OverlapDepth( dimensions_t rhs ){
    vec2_t* overlap = new vec2_t;
    
    overlap->Clear();
    
    //Push left
    if ( left < rhs.left ) {
        overlap->x.current.value = ( right - rhs.left );
    }
    
    //Push right
    if ( right > rhs.right ){
        overlap->x.current.value = -( rhs.right - left );
    }
    
    //Push Up
    if ( top > rhs.top ){
        overlap->y.current.value = ( top - rhs.bottom );
    }
    
    //Push Down
    if ( bottom < rhs.bottom ){
        overlap->y.current.value = -( rhs.top - bottom );
    }
    return overlap;
}




#ifdef APPLE
#pragma mark momentum_t
#endif
/*
 ====================
 momentum_t::Speed
 ====================
 */
bounds_t* momentum_t::Speed( void ){
    return &speed;
}



/*
 ====================
 momentum_t::GetSpeed
 ====================
 */
const float momentum_t::GetSpeed( void ) const{
    return speed.current.value;
}



/*
 ====================
 momentum_t::Clear
 ====================
 */
void momentum_t::Clear( void ){
    speed.Clear();
    accel.Clear();
    decel.Clear();
}


/*
 ====================
 momentum_t::Accelerate
 ====================
 */
void momentum_t::Accelerate( const Uint32 delta ){
    accel.Increase();
    decel.Decrease();
    decel.CheckBounds();
    accel.CheckBounds();
}


/*
 ====================
 momentum_t::Decelerate
 ====================
 */
void momentum_t::Decelerate( const Uint32 delta ){
    decel.Increase();
    accel.Decrease();
    decel.CheckBounds();
    accel.CheckBounds();
}


/*
 ====================
 momentum_t::Update
 ====================
 */
void momentum_t::Update( const Uint32 delta ){
    speed.current.value -= decel.current.value * delta;
    speed.CheckBounds();
    
    speed.current.value += accel.current.value * delta;
    speed.CheckBounds();
    
    decel.Decrease();
    accel.Decrease();
    decel.CheckBounds();
    accel.CheckBounds();
}


/*
 ====================
 momentum_t::Stop
 ====================
 */
void momentum_t::Stop( void ){
    speed.current.value = 0;
}



/*
 ====================
 momentum_t::InMotion
 ====================
 */
const bool momentum_t::InMotion( void ) const{
    return ( GetSpeed() > 0);
}


/*
 ====================
 momentum_t::PrintOut
 ====================
 */
void momentum_t::PrintOut( void ) const{
    cout << "Value: " ;
    speed.PrintOut();
    cout << endl;

}


/*
 ====================
 momentum_t::operator=
 ====================
 */
momentum_t* momentum_t::operator=( const momentum_t rhs ){
    speed = rhs.speed;
    accel = rhs.accel;
    decel = rhs.decel;

    return this;
}







































