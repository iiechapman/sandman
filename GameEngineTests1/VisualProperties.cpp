//
//  VisualProperties.cpp
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "VisualProperties.h"
using namespace std;

#ifdef APPLE
#pragma mark cell_t
#endif

/*
 ====================
 cell_t::cell_t
 ====================
 */
cell_t::cell_t(){
    Clear();
}



/*
 ====================
 cell_t::Clear
 ====================
 */
void cell_t::Clear( void ){
    row     =   0;
    column  =   0;
}



/*
 ====================
 cell_t::PrintOut
 ====================
 */
void cell_t::PrintOut( void ){
    cout << "Row: " << row;
    cout << "Column: " << column;
}



#ifdef APPLE
#pragma mark color_t
#endif

/*
 ====================
 color_t::color_t
 ====================
 */
color_t::color_t(){
    Clear();
}



/*
 ====================
 color_t::ToColor
 ====================
 */
SDL_Color* color_t::ToColor( void ){
    SDL_Color* newColor = new SDL_Color;
    
    newColor->r         = red;
    newColor->g         = green;
    newColor->b         = blue;
    newColor->a         = alpha;
    
    return newColor;
}



/*
 ====================
 color_t::Clear
 ====================
 */
void color_t::Clear( void ){
    red = 0;
    blue = 0;
    green = 0;
    alpha = 0;
}


/*
 ====================
 color_t::PrintOut
 ====================
 */
void color_t::PrintOut( void ){
    cout << "Red: " << red;
    cout << "Green: " << green;
    cout << "Blue: " << blue;
    cout << "Alpha: " << alpha;
}
