//
//  InputProperties.cpp
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "InputProperties.h"


#ifdef APPLE
#pragma mark direction_t
#endif


/*
 ====================
 direction_t::direction_ts
 ====================
 */
direction_t::direction_t(){
    direction = DIRECTION_NONE;
}


/*
 ====================
 direction_t::PrintOut
 ====================
 */
void direction_t::PrintOut( void ){
    switch ( direction ) {
        case DIRECTION_DOWN:
            cout << "Down Direction" << endl;
            break;
            
        case DIRECTION_UP:
            cout << "Up Direction" << endl;
            break;
            
        case DIRECTION_LEFT:
            cout << "Left Direction" << endl;
            break;
            
        case DIRECTION_RIGHT:
            cout << "Right Direction" << endl;
            break;
            
            
        case DIRECTION_NONE:
            cout << "No Direction" << endl;
            break;
            
            
        case DIRECTION_NULL:
            cout << "Null Direction" << endl;
            break;
            
        default:
            break;
    }
}


#ifdef APPLE
#pragma mark button_t
#endif


/*
 ====================
 button_t::Clear
 ====================
 */
void button_t::Clear( void ){
    position.Clear();
    state = BUTTON_DISABLED;
    name = "Unnasigned Button";
}


/*
 ====================
 button_t::PrintOut
 ====================
 */
void button_t::PrintOut( void ){
    cout << "name: " << name << endl;
    cout << "state: " ;
    
    switch (state) {
        case BUTTON_DISABLED:
            cout << "Disabled";
            break;
            
        case BUTTON_ENABLED:
            cout << "Enabled";
            break;
            
        case BUTTON_PRESSED:
            cout << "Pressed";
            break;
            
        case BUTTON_HELD:
            cout << "Held";
            break;
            
        case BUTTON_RELEASED:
            cout << "Released";
            break;
            
        default:
            break;
    }
    
    cout << endl;
    
    
    position.PrintOut();
    
}



/*
 ====================
 button_t::Press
 ====================
 */
void button_t::Press( void ){
    if ( state != BUTTON_DISABLED ){
        if ( state == BUTTON_HELD || state == BUTTON_PRESSED ){
            state = BUTTON_HELD;
        } else {
            state = BUTTON_PRESSED;
        }
    }
    
}



/*
 ====================
 button_t::Release
 ====================
 */
void button_t::Release( void ){
    if ( state != BUTTON_DISABLED ){
        state = BUTTON_RELEASED;
    }
}


/*
 ====================
 button_t::Enable
 ====================
 */
void button_t::Enable( void ){
    state = BUTTON_ENABLED;
}


/*
 ====================
 button_t::Disable
 ====================
 */
void button_t::Disable( void ){
    state = BUTTON_DISABLED;
}



/*
 ====================
 button_t::Active
 ====================
 */
const bool button_t::Active( void ) const{
    return ( state == BUTTON_PRESSED || state == BUTTON_HELD );
}



/*
 ====================
 button_t::Pressed
 ====================
 */
const bool button_t::Pressed( void ) const{
    return ( state == BUTTON_PRESSED );
}


/*
 ====================
 button_t::Held
 ====================
 */
const bool button_t::Held( void )const{
    return ( state == BUTTON_HELD );
}



/*
 ====================
 button_t::Released
 ====================
 */
const bool button_t::Released( void ) const {
    return ( state == BUTTON_RELEASED );
}


/*
 ====================
 button_t::SinglePress
 ====================
 */
const bool button_t::SinglePress( void ) const{
    return ( ( state == BUTTON_PRESSED ) && ( state != BUTTON_HELD ) );
}


#ifdef APPLE
#pragma mark controller_t
#endif


/*
 ====================
 controller_t::controller_t
 ====================
 */
controller_t::controller_t(){
    up.name = "Button Up";
    down.name = "Button Down";
    left.name = "Button Left";
    right.name = "Button Right";
    
    button1.name = "Button 1";
    button2.name = "Button 2";
}

/*
 ====================
 controller_t::Printout
 ====================
 */
void controller_t::PrintOut( void ){
    up.PrintOut();
    down.PrintOut();
    left.PrintOut();
    right.PrintOut();
}


/*
 ====================
 controller_t::NoDirectionPressed
 ====================
 */
const bool controller_t::NoDirectionPressed( void ) const{
    return ( up.Released()   &&
            down.Released() &&
            left.Released() &&
            right.Released() );
}

































