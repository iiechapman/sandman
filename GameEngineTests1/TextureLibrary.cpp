//
//  TextureLibrary.cpp
//  Sandman
//
//  Created by Evan Chapman on 1/10/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "TextureLibrary.h"


/*
 ====================
 TextureLibrary::TextureLibrary
 ====================
 */
TextureLibrary::TextureLibrary(){
    
}

/*
 ====================
 TextureLibrary::~TextureLibrary
 ====================
 */
TextureLibrary::~TextureLibrary(){

}


/*
 ====================
 TextureLibrary::GetIndexByName
 ====================
 */
const int TextureLibrary::GetIndexByName( const string file ) const{
    //Returns index of texture with given name
    for ( int i = 0 ; i < MAX_TEXTURES ; i++ ){
        if ( textureName[ i ] == file ){
            return i;
        }
    }
    return -1;
}

/*
 ====================
 TextureLibrary::TextureExistsWithIndex
 ====================
 */
bool TextureLibrary::TextureExistsWithIndex( const int index ) const{
    return texture[index];
}


/*
 ====================
  TextureLibrary::TextureExistsWithName
 ====================
 */
bool TextureLibrary::TextureExistsWithName( const string file ) const{
    for ( int i = 0 ; i < MAX_TEXTURES ; i++ ){
        if ( textureName[ i ] == file ){
            return true;
        }
    }
    return false;
}


/*
 ====================
  TextureLibrary::InitWithRenderer
 ====================
 */
void TextureLibrary::InitWithRenderer(SDL_Renderer* selectedRenderer){
    currentRenderer = selectedRenderer;
    freeTexture = 0;
    isFull = false;
    resetAtFull = false;
}


/*
 ====================
  TextureLibrary::GetTextureByIndex
 ====================
 */
SDL_Texture* TextureLibrary::GetTextureByIndex(const int index) const{
    return texture[ index ];
}


/*
 ====================
  TextureLibrary::GetNameByIndex
 ====================
 */
const string TextureLibrary::GetNameByIndex( const int index ) const{
    return textureName[ index ];
}


/*
 ====================
  TextureLibrary::ClearLibrary
 ====================
 */
void TextureLibrary::ClearLibrary( void ){
    for ( int i = 0 ; i < MAX_TEXTURES ; i++ ){
        texture[ i ] = 0;
        textureName[ i ] = "";
    }
}


/*
 ====================
  TextureLibrary::CreateTextureFromFile
 ====================
 */
const int TextureLibrary::CreateTextureFromFile(const string file){
    int currentTextureIndex = freeTexture;
    
    //Load file into surface
    loadingSurface = IMG_Load( file.c_str() );
    if ( !loadingSurface ){
        printf( "Error loading file: %s \n" , IMG_GetError() );
        return -1;
    }
    
    //Select last free texture, convert surface to texture
    texture[ currentTextureIndex ] = SDL_CreateTextureFromSurface( currentRenderer, loadingSurface );
    
    //If error print message otherwise store texture
    if (!texture[ currentTextureIndex ]){
        printf( "Error making texture: %s \n", IMG_GetError() );
        return -1;
    } else {
        //Save name of file to name for checking later
        textureName[ currentTextureIndex ] = file;
        
        //Update free texture, check for bounds, clamp if needed
        freeTexture++;
        if ( freeTexture > MAX_TEXTURES ){
            if ( resetAtFull ){
                isFull = false;
                freeTexture = 0;
            } else {
                isFull = true;
                freeTexture = MAX_TEXTURES;
            }
        }
    }
    
    SDL_FreeSurface(loadingSurface);
    
    return currentTextureIndex;
}
























