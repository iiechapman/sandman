//
//  TextureLibrary.h
//  Sandman
//
//  Created by Evan Chapman on 1/10/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __Sandman__TextureLibrary__
#define __Sandman__TextureLibrary__

#include <iostream>
#include "PrefixHeader.pch"
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <string>
using namespace std;

const int MAX_TEXTURES = 256;

/*
 ===============================================================================
 
 TextureLibrary
 Contains a list of textures access by index or name
 
 ===============================================================================
 */
class TextureLibrary{
public:
                            TextureLibrary();
                            ~TextureLibrary();
    
    void                    InitWithRenderer(SDL_Renderer* selectedRenderer);
    
    void                    ClearLibrary( void );
    const int               CreateTextureFromFile( const string file );
    
    const string            GetName( const int index ) const;
    SDL_Texture*            GetTextureByIndex( const int index ) const;
    const string            GetNameByIndex( const int index ) const;
    const int               GetIndexByName( const string file ) const;
    
    bool                    TextureExistsWithIndex( const int index ) const;
    bool                    TextureExistsWithName( const string file ) const;
    
private:
    SDL_Renderer*           currentRenderer;
    int                     freeTexture;
    
    bool                    isFull;
    bool                    resetAtFull;
    
    SDL_Surface*            loadingSurface;
    SDL_Texture*            texture[ MAX_TEXTURES ];
    string                  textureName[ MAX_TEXTURES ];
};






#endif /* defined(__Sandman__TextureLibrary__) */
