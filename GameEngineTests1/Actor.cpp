//
//  Actor.cpp
//  Sandman
//
//  Created by Evan Chapman on 1/10/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "Actor.h"

#ifdef APPLE
#pragma mark Head
#endif


/*
 ====================
 Actor::Actor
 ====================
 */
Actor::Actor(){
        if ( DEBUG_MODE ){
            cout << "Actor Created" << endl;
        }
    Init();
}



/*
 ====================
 Actor::~Actor
 ====================
 */
Actor::~Actor(){
        if ( DEBUG_MODE ){
            cout << "Actor Destroyed" << endl;
        }
}



/*
 ====================
 Actor::Init
 ====================
 */
void Actor::Init( void ){
    //Initialize actor to defaults

    dimensions.Clear();
    center.x = 0;
    center.y = 0;
    angle = 0;
    flip = SDL_FLIP_NONE;
    momentum.Clear();
    texture = 0;
    source.Clear();
    dimensions.Clear();
    isMoving = false;
    
}


/*
 ====================
 Actor::PrintOut
 ====================
 */
void Actor::PrintOut(){
    cout << "Texture: " << texture << endl;

    cout << "Tint: " ;
    tint.PrintOut();
    cout << endl;
    
    cout << "Source:";
    source.PrintOut();
    cout << endl;
    
    cout << "Dimensions: ";
    dimensions.PrintOut();
    cout << endl;
    
    cout << "Speed: ";
    momentum.PrintOut();
    cout << endl;
    
    cout << "Is Moving: " << isMoving << endl;
    
    cout << "Direction: ";
    direction.PrintOut();
    

}


/*
 ====================
 Actor::Update
 ====================
 */
//Updates actor based on behaviors
void Actor::Update( const Uint32 delta ) {
    momentum.Update( delta );
    HandleInput( delta );
    UpdateMovement( delta );
    UpdateAnimation( delta );
    UpdateBoundingBox();
}


/*
 ====================
 Actor::Correct
 ====================
 */
void Actor::Correct( const Uint32 delta ){
    
}


/*
 ====================
 Actor::CheckCollision
 ====================
 */
void Actor::CheckCollisions( const Actor* rhs ){

    if ( boundingBox.OverlapWithDimensions( rhs->boundingBox )){
        dimensions.x -= lastMove.x.current.value;
        dimensions.y -= lastMove.y.current.value;
    }
    
}


/*
 ====================
 Actor::SetTextureLibrary
 ====================
 */
bool Actor::SetTextureLibrary( TextureLibrary* textureLib ){
    if (textureLib){
        pCurrTextureLib = textureLib;
        return true;
    }
    return false;
}


/*
 ====================
 Actor::SetRenderer
 ====================
 */
bool Actor::SetRenderer( SDL_Renderer* renderer ){
    pCurrRenderer = renderer;
    if (renderer){
        return true;
    } else {
        return false;
    }
}


/*
 ====================
 Actor::SetDimensions
 ====================
 */
void Actor::SetDimensions( dimensions_t newDimensions ){
    dimensions = newDimensions;
}


/*
 ====================
 Actor::SetDirection
 ====================
 */
void Actor::SetDirection( direction_e newDirection ){
    direction.direction = newDirection;
}


/*
 ====================
 Actor::GetDimensions
 ====================
 */
dimensions_t* Actor::GetDimensions( void ){
    return &dimensions;
}


/*
 ====================
 Actor::SetBoundingBox
 ====================
 */
void Actor::SetBoundingBox( const dimensions_t newBox ){
    boundingBox = newBox;
}


/*
 ====================
 Actor::SetSource
 ====================
 */
void Actor::SetSource( dimensions_t newSource ){
    source = newSource;
}


/*
 ====================
 Actor::SetTint
 ====================
 */
void Actor::SetTint( color_t newTint ){
    tint = newTint;
}



/*
 ====================
 Actor::SetMomentum
 ====================
 */
void Actor::SetMomentum( const momentum_t newMomentum ){
    momentum = newMomentum;
}


/*
 ====================
 Actor::GetMomentum
 ====================
 */
momentum_t* Actor::GetMomentum( void ){
    return &momentum;
}



/*
 ====================
 Actor::Render
 ====================
 */
void Actor::Render( void ){
    //Set Tint
    SDL_SetTextureColorMod(pCurrTextureLib->
                           GetTextureByIndex(texture), tint.red, tint.green, tint.blue);
    
    SDL_Rect renderSource = source.ToRect();
    SDL_Rect renderDimensions = dimensions.ToRect();
    
    //Render to current renderer surface
    SDL_RenderCopyEx( pCurrRenderer, pCurrTextureLib->GetTextureByIndex( texture ),&renderSource, &renderDimensions, angle, &center, flip );
}


/*
 ====================
 Actor::SetTextureWithIndex
 ====================
 */
//Set Texture to index if texture exists
bool Actor::SetTextureWithIndex( const int index ){
    if ( pCurrTextureLib->TextureExistsWithIndex( index ) ){
        texture = index;
        return 0;
    } else {
        texture = -1;
        return false;
    }
    return true;
}


/*
 ====================
 Actor::SetController
 ====================
 */
void Actor::AttachController( controller_t* newController ){
    controller = newController;
}

/*
 ====================
 Actor::DetachController
 ====================
 */
void Actor::DetachController( void ){
    controller = NULL;
}



/*
 ====================
 Actor::SetTextureWithFile
 ====================
 */
//creates texture from file, checks if already loaded
bool Actor::SetTextureWithFile( const string file ){
    if ( pCurrTextureLib->TextureExistsWithName( file ) ){
        texture = pCurrTextureLib->GetIndexByName( file );
    } else {
        pCurrTextureLib->CreateTextureFromFile( file );
    }
    return 0;
}



/*
 ====================
 Actor::HandleInput
 ====================
 */
void Actor::HandleInput( Uint32 delta ){
    if ( controller ){
        if ( controller->NoDirectionPressed() ){
            if ( momentum.InMotion() ){
                momentum.Decelerate( delta );
            }
        }
        
        if ( !controller->NoDirectionPressed() ){
            momentum.Accelerate( delta );
        }
        
        if ( controller->up.Pressed() ){
            direction.direction = DIRECTION_UP;
        }
        
        if ( controller->down.Pressed() ){
            direction.direction = DIRECTION_DOWN;
        }
        
        if ( controller->left.Pressed() ){
            direction.direction = DIRECTION_LEFT;
        }
        
        if ( controller->right.Pressed() ){
            direction.direction = DIRECTION_RIGHT;
        }
    }
}


/*
 ====================
 Actor::UpdateBoundingBox
 ====================
 */
void Actor::UpdateBoundingBox( void ){
    boundingBox.x = dimensions.x;
    boundingBox.y = dimensions.y;
}


/*
 ====================
 Actor::UpdateMovement
 ====================
 */
void Actor::UpdateMovement( Uint32 delta ){
    switch ( direction.direction ) {
        case DIRECTION_RIGHT:
            lastMove.y.current.value = 0;
            lastMove.x.current.value = momentum.GetSpeed() * delta;
            dimensions.x += lastMove.x.current.value;
            break;
            
        case DIRECTION_LEFT:
            lastMove.y.current.value = 0;
            lastMove.x.current.value = -momentum.GetSpeed() * delta;
            dimensions.x += lastMove.x.current.value;
            break;
            
        case DIRECTION_DOWN:
            lastMove.x.current.value = 0;
            lastMove.y.current.value = momentum.GetSpeed() * delta;
            dimensions.y += lastMove.y.current.value;
            break;
            
        case DIRECTION_UP:
            lastMove.x.current.value = 0;
            lastMove.y.current.value = -momentum.GetSpeed() * delta;
            dimensions.y += lastMove.y.current.value;
            break;
            
        default:
            break;
    }
}


/*
 ====================
 Actor::SetCharacterNum
 ====================
 */
void Actor::SetCharacterNum( const int newCharacterNum ){
    characterNum = newCharacterNum;
}


/*
 ====================
 Actor::UpdateAnimation
 ====================
 */
void Actor::UpdateAnimation( Uint32 delta ){
    switch ( direction.direction ) {    //Change animation based on direction
        case DIRECTION_RIGHT:
            currAnimRow =   rightAnimRow;
            currAnimCol =   rightAnimCol;
            break;
            
        case DIRECTION_LEFT:
            currAnimRow =   leftAnimRow;
            currAnimCol =   leftAnimCol;
            break;
            
        case DIRECTION_DOWN:
            currAnimRow =   downAnimRow;
            currAnimCol =   downAnimCol;
            break;
            
        case DIRECTION_UP:
            currAnimRow =   upAnimRow;
            currAnimCol =   upAnimCol;
            break;
            
        default:
            currAnimRow =   rightAnimRow;
            currAnimCol =   rightAnimCol;
            break;
    }
    
    
    if ( momentum.InMotion())//Change speed and position based on if moving
    {
        animTickTotal+=animTickSpeed * delta;//Check if frame should elapse
        
        if( animTickTotal >= animTickMax )
        {
            animTickTotal = 0;
            currFrame++;
        }
        
        if (currFrame >= lastFrame)
        {
            currFrame = 1;
        }
    } else {
        currFrame = 0;
    }
    
    column  =   currAnimCol[currFrame];
    row     =   currAnimRow[currFrame];
    
    int characterColumn = characterNum % maxColumns;
    int characterRow    = characterNum / maxRows;
    
    int finalColumn = column + (characterColumn * columnStride);
    int finalRow    = row + (characterRow * rowStride );
    
    source.x = xSize * finalColumn;
    source.y = ySize * finalRow;
    source.w = xSize;
    source.h = ySize;
}























































