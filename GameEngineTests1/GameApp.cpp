//
//  GameApp.cpp
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/30/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.


#include <vector>
#include "GameApp.h"
#include <SDL2_image/SDL_image.h>
#include <iostream>
#include <string>

using namespace std;

/*
 ===============================================================================
 
 GameApp.cpp
 Contains main game loop and connects classes together
 
 ===============================================================================
 */

#ifdef APPLE
#pragma mark GameApp
#endif

/*
 ====================
 GameApp::GameApp
 ====================
 */
GameApp::GameApp(){
    if ( DEBUG_MODE ){
        cout << "Begin Game " << endl;
    }
}


/*
 ====================
 GameApp::~GameApp
 ====================
 */
GameApp::~GameApp(){
    if ( DEBUG_MODE ){
        cout << "Ending game " << endl;
        cout << "Destroying actors" << endl;
        DestroyActors();
        cout << "End " << endl;
    }
}

/*
 =====================
 GameApp::Destroy
 =====================
 */

void GameApp::Destroy( void )
{
    App::destroy();
}


#ifdef APPLE
#pragma mark Init
#endif
/*
 =====================
 GameApp::Init
 =====================
 */
const int GameApp::Init( void )
{
    srand ( int ( time(NULL)) );
    
    controller = new controller_t();
    SetupTextureLibrary();
    SetupTimer();
    LoadAssetts();
    SetupTestData();
    CreateActors();
    
    return APP_OK;
}


#ifdef APPLE
#pragma mark Game Loop
#endif

/*
 =====================
 GameApp::Run
 =====================
 */
const int GameApp::Run( int width, int height, string title ){
    App::run( width,height ,title );
    running = true;
    
    Init();
    LoadXML();
    
    //Game Loop
    while ( running )
    {
        timer.Update();
        
        //Live Inject of XML
        if ( loadScripts ){
            LoadXML();
        }
        
        SDL_Event ev;
        while ( SDL_PollEvent( &ev ) ){
            HandleEvents( &ev );
        }
        
        HandleKeyboard();
        HandleLogic();
        
        if ( !paused ){
            //TODO: Put in actor class
            RandomizeMovement();
            UpdateActors();
        }
        
        Render();
        RegulateFrameRate();
        CalculateFPS();

        if ( running == false ){
            break;
        }
    }
    return APP_OK;
}


#ifdef APPLE
#pragma mark Events
#endif

/*
 ====================
 GameApp::HandleKeyboard
 ====================
 */
void GameApp::HandleKeyboard( void ){
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    
    if ( state[ SDL_SCANCODE_W ] ) {
        controller->button1.Press();
    } else {
        controller->button1.Release();
    }
    
    if ( state[ SDL_SCANCODE_S ] ) {
        controller->button2.Press();
    } else {
        controller->button2.Release();
    }
    
    if ( state[ SDL_SCANCODE_F ] ) {
        controller->fullScreenButton.Press();
    } else {
        controller->fullScreenButton.Release();
    }
    
    if ( state[ SDL_SCANCODE_SPACE ] ) {
        //controller->button1.Press();
    } else {
        //controller->button1.Release();
    }
    
    if ( state[ SDL_SCANCODE_UP ] ) {
        controller->up.Press();
    } else {
        controller->up.Release();
    }
    
    if ( state[ SDL_SCANCODE_DOWN ] ) {
        controller->down.Press();
    } else {
        controller->down.Release();
    }
    
    if ( state[ SDL_SCANCODE_RIGHT ] ) {
        controller->right.Press();
    } else {
        controller->right.Release();
    }
    
    if ( state[ SDL_SCANCODE_LEFT ] ) {
        controller->left.Press();
    } else {
        controller->left.Release();
    }
}


/*
 ====================
 GameApp::HandleEvents
 ====================
 */
void GameApp::HandleEvents( SDL_Event* ev ){
    App::HandleEvents( ev );
}



#ifdef APPLE
#pragma mark Render
#endif
/*
 ====================
 GameApp::Render
 ====================
 */
void GameApp::Render(){
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderSetScale(renderer, scale, scale);
    
    //TODO:Move out to specific classes
    SDL_RenderCopyEx( renderer, textureLib->GetTextureByIndex( backTexture ), NULL, &backDim , angle, &center, flip );
    
    
    RenderActors();
    App::Render();
}



#ifdef APPLE
#pragma mark Actors
#endif

/*
 ====================
 GameApp::SetupTestActor
 ====================
 */
void GameApp::SetupTestData( void ){
    momentum.speed.SetMax(.32);
    momentum.speed.SetMin(0);
    momentum.speed.Set(0);
    
    
    momentum.accel.SetMax(.1);
    momentum.accel.SetMin(0);
    momentum.accel.Set(0);
    momentum.accel.current.SetChangeValue(.005);
    
    momentum.decel.SetMax(.0025);
    momentum.decel.SetMin(0);
    momentum.decel.Set(0);
    momentum.decel.current.SetChangeValue(.0015);
    
}



/*
 =====================
 GameApp::CreateActors
 =====================
 */
void GameApp::CreateActors( void ){
    //Test Actor
    if (totalActors > MAX_ACTORS){
        totalActors = MAX_ACTORS;
    }
    
    for ( int index = 0 ; index <= totalActors; index ++ ) {
        testActor[ index ] = new Actor();
        testActor[ index ] ->SetRenderer( renderer );
        testActor[ index ] ->SetTextureLibrary( textureLib );
        testActor[ index ] ->SetTextureWithIndex( spriteTexture );
        
        if ( index != 0 ){
            testActor[ index ]->DetachController();
        }
        
        testActor[ 0 ]->AttachController( controller );
   
        
        momentum_t newMomentum = momentum;
        
        newMomentum.speed.max = ( .09 + ( rand() % RAND_MAX ) * .0000000003);
        
        testActor[ index ]->SetMomentum( newMomentum );
        
        dimensions_t dimensions;
        dimensions.x = (rand() % 800);
        dimensions.y = (rand() % 800);
        dimensions.w = 80;
        dimensions.h = 80;
        testActor[ index ]->SetDimensions( dimensions );
        
        dimensions_t boundingBox;
        boundingBox.x = 0;
        boundingBox.y = 0;
        boundingBox.w = 32;
        boundingBox.h = 32;
        testActor[ index ]->SetBoundingBox( boundingBox );
        
        color_t tint;
        tint.red    = 255;
        tint.green  = 255;
        tint.blue   = 255;
        testActor[ index ]->SetTint( tint );
        
        dimensions_t newSource;
        newSource.x = 0;
        newSource.y = 0;
        newSource.w = 32;
        newSource.h = 32;
        testActor[ index ]->SetSource( newSource );
        
        testActor[ index ]->SetCharacterNum( rand() % 50 );
    }
}

/*
 ====================
 GameApp::UpdateActors
 ====================
 */
void GameApp::UpdateActors( void ){
    for ( int index = 0 ; index < totalActors ;index++ ){
        testActor[ index ]->Update( timer.Delta() );
        
        for ( int uindex = 0 ; uindex < totalActors ; uindex++ ){
            if ( testActor[ index ] != testActor[ uindex ] ){
                testActor[ index ]->CheckCollisions( testActor[ uindex ] );
            }
        }
    }
}


/*
 ====================
 GameApp::RenderActors
 ====================
 */
void GameApp::RenderActors( void ){
    for ( int index = 0  ; index < totalActors; index ++ ) {
        testActor[ index ]->Render();
    }
}



/*
 =====================
 GameApp::DestroyActors
 =====================
 */
void GameApp::DestroyActors( void ){
    for ( int index = 0  ; index < totalActors; index ++ ) {
        if ( testActor [ index ] ){
            delete testActor[ index ];
            testActor[ index ] = 0;
        }
    }
}



#ifdef APPLE
#pragma mark File Loading
#endif
/*
 =====================
 GameApp::LoadXML
 =====================
 */
void GameApp::LoadXML( void ){
    delete doc;
    doc = 0;
    doc = new XMLDocument();
    
    FILE* file = fopen( "test.xml", "r" );
    
    if ( doc->LoadFile(file) == XML_NO_ERROR ){
        
        XMLNode* root = doc->FirstChild();
        
        float* currentFloatValue = NULL;
        int* currentIntValue = NULL;
        
        bool typeFloat = false;
        bool typeInt = false;
        
        
        if ( root != NULL )   //If we find root, parse through elements
        {
            
            for ( XMLElement* elem = root->FirstChildElement() ; elem != NULL ; elem = elem->NextSiblingElement() ){            //Traverse through base Nodes
                
                if ( DEBUG_MODE ){
                    cout << "=====================" << endl;
                    string elemName = elem->Value();
                    cout << elemName << endl;
                }
                bool inject = false;
                
                //Traverse values inside nodes
                for ( XMLNode* node = elem->FirstChild() ; node !=NULL ; node = node->NextSibling() ){
                    
                    string tag = node->ToElement()->Value();
                    string element = node->ToElement()->GetText();
                    
                    //Set type
                    if ( element == "float" ){
                        typeFloat = true;
                        typeInt = false;
                    }
                    
                    if ( element == "int" ){
                        typeFloat = false;
                        typeInt = true;
                    }
                    
                    //Set value to alter
                    if ( element == "player_speed_accel" ){
                        inject = true;
                        if ( DEBUG_MODE ){
                            cout << "Selected player_speed_accel" << endl;
                        }
                        if ( typeFloat ){
                            //currentFloatValue = &momentum.accel;
                        }
                    }
                    
                    
                    if (element == "player_speed_decel"){
                        inject = true;
                        if ( DEBUG_MODE ){
                            cout << "Selected player_speed_decel" << endl;
                        }
                        if ( typeFloat ){
                            //currentFloatValue = &playerSpeed.decel;
                        }
                    }
                    
                    if (element == "player_speed_max"){
                        inject = true;
                        if ( DEBUG_MODE ){
                            cout << "Selected player_speed_max" << endl;
                        }
                        if ( typeFloat ){
                            //currentFloatValue = &playerSpeed.bounds.max;
                        }
                    }
                    
                    
                    
                    if (element == "total_actors"){
                        inject = true;
                        if ( DEBUG_MODE ){
                            cout << "Selected total actors" << endl;
                        }
                        DestroyActors();
                        if ( typeInt ){
                            //currentIntValue = &totalActors;
                        }
                    }
                    
                    
                    if (element == "create_actors"){
                        CreateActors();
                    }
                    
                    //Value Set Section
                    if (tag == "val" && inject){
                        inject = false;
                        if ( DEBUG_MODE ){
                            cout << "Injecting Value " << endl;
                        }
                        if (typeFloat){
                            //(*currentFloatValue) = stof(element);
                            if ( DEBUG_MODE ){
                                cout << "Change value to ";
                                cout << (*currentFloatValue) << endl;
                            }
                        }
                        
                        if (typeInt){
                            //(*currentIntValue) = stof(element);
                            if ( DEBUG_MODE ){
                                cout << "Change value to ";
                                cout << (*currentIntValue) << endl;
                            }
                        }
                    }
                    
                    if ( DEBUG_MODE ){
                        cout << tag << ": ";
                        cout << element << endl;
                    }
                }
            }
            if ( DEBUG_MODE ){
                cout << "=====================" << endl;
            }
        }
    }
    fclose( file );
    doc->Clear();
}



/*
 =====================
 GameApp::LoadAssetts
 =====================
 */
const int GameApp::LoadAssetts( void ){    //Load Assetts into app, used now for testing
TestTextureLibrary:{
    spriteTexture = textureLib->CreateTextureFromFile( "art/pokemonsprites.png" );
    backTexture = textureLib->CreateTextureFromFile( "art/testtown1.png" );
}
    
GlobalVariables:{    
    //Set background dimensions
    backDim.x = -500;
    backDim.y = -50;
    backDim.w = 1800;
    backDim.h = 1000;
}
    return APP_OK;
}


#ifdef APPLE
#pragma mark Debug
#endif


/*
 ====================
 GameApp::RegulateFrameRate
 ====================
 */
void GameApp::RegulateFrameRate( void ){
    Uint32 timePerFrame = 1000 / targetFrameRate - 2;
    
    if ( timer.Delta() < timePerFrame ){
        SDL_Delay( timePerFrame - timer.Delta() );
    }
}



/*
 ====================
 GameApp::CalculateFPS
 ====================
 */
void GameApp::CalculateFPS( void ){
    framesRendered++;
    
    if ( timer.Tick() ){
        paused = false;
        timer.Reset();
        framesPerSecond = framesRendered / 2;
        framesRendered = 0;
        char buffer[100];
        
        sprintf( buffer, "Sandman FPS: %i" , framesPerSecond );
        
        SDL_SetWindowTitle( window, buffer );
        timer.Start();
    }
}


/*
 ====================
 GameApp::HandleLogic
 ====================
 */
void GameApp::HandleLogic( void ){
    CheckForFullScreen();
    CharacterSwitch();
    CheckActorBounds();
}


/*
 ====================
 GameApp::CheckForFullScreen
 ====================
 */
void GameApp::CheckForFullScreen( void  ){
    if ( controller->fullScreenButton.SinglePress() ){
        cout << "Pressed" << endl;
        if ( fullScreen ){
            paused = true;
            fullScreen = false;
            windowMode = static_cast<SDL_WindowFlags>(0);
        } else {
            paused = true;
            fullScreen = true;
            windowMode = SDL_WINDOW_FULLSCREEN;
        }
        SDL_SetWindowFullscreen(window, windowMode );
    }
}



/*
 ====================
 GameApp::SetupTimer
 ====================
 */
void GameApp::SetupTimer( void ){
    timer.SetTickTime( 1000 );
    timer.Start();
    timer.SetLooping( true );
}

/*
 ====================
 GameApp::SetupTextureLibrary
 ====================
 */
void GameApp::SetupTextureLibrary( void ){
    //TODO:Move out to Texturehandler
    textureLib = new TextureLibrary();
    textureLib->InitWithRenderer( renderer );
}


/*
 ====================
 GameApp::InitTextureHandling
 ====================
 */
void GameApp::InitTextureHandling( void ){
    if ( !IMG_Init( IMG_INIT_PNG ) ){
        printf( "Problem with png: %s \n" , IMG_GetError() );
    }
    SetupTextureLibrary();
}


/*
 ====================
 GameApp::CharacterSwitch
 ====================
 */
void GameApp::CharacterSwitch( void ){
    
    color_t normal;
    normal.red = 100;
    normal.blue = 100;
    normal.green = 100;
    
    
    color_t highlight;
    highlight.blue = 255;
    highlight.red = 255;
    highlight.green = 255;
    
    
    if ( controller->button1.SinglePress() ) {
        testActor[ characterNum ]->DetachController();

        characterNum++;
        if ( characterNum > totalActors ){
            characterNum = 0;
        }
        testActor[ characterNum ]->AttachController( controller );

        
    }
    
    if ( controller->button2.SinglePress() ) {
        testActor[ characterNum ]->DetachController();

        characterNum--;
        if ( characterNum < 0){
            characterNum = totalActors;
        }
        testActor[ characterNum ]->AttachController( controller );

    }
}


/*
 ====================
 GameApp::CheckActorBounds
 ====================
 */
void GameApp::CheckActorBounds( void ){
    for ( int index = 0 ; index < totalActors ; index++ ){
        if ( testActor[ index ]->GetDimensions()->x > SCREEN_WIDTH + 40 ){
            testActor[ index ]->GetDimensions()->x = -40;
        }
        
        if ( testActor[ index ]->GetDimensions()->x < -40 ){
            testActor[ index ]->GetDimensions()->x = SCREEN_WIDTH + 40;
        }
        
        
        if ( testActor[ index ]->GetDimensions()->y < -40 ){
            testActor[ index ]->GetDimensions()->y = SCREEN_HEIGHT + 40;
        }
        
        if ( testActor[ index ]->GetDimensions()->y > SCREEN_HEIGHT + 40 ){
            testActor[ index ]->GetDimensions()->y = -40;
        }
    }
}


/*
 ====================
 GameApp::RandomizeMovement
 ====================
 */
void GameApp::RandomizeMovement( void ){
    
    int select =1 + rand() % totalActors;
    
    int change = rand() % (totalActors * 2);
    
    if ( change == 1){
        int direction = rand() % 4;
        
        switch (direction) {
            case 1:
                testActor[ select ]->SetDirection( DIRECTION_RIGHT );
                break;
                
            case 2:
                testActor[ select ]->SetDirection( DIRECTION_LEFT );
                break;
                
            case 3:
                testActor[ select ]->SetDirection( DIRECTION_UP );
                break;

            case 4:
                testActor[ select ]->SetDirection( DIRECTION_DOWN );
                break;
                
            default:
                break;
        }
        
    }
    
    
    if (change == 2 ){
        testActor[ select ]->GetMomentum()->speed.current.value = .1 + (rand() % RAND_MAX) * .0000000001;
    }
    
    if (change == 3 ){
        testActor[ select ]->GetMomentum()->speed.current.value = 0;
    }
    
    
}

















































